/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jhconnection;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Display;
import javax.microedition.midlet.*;
import tn.harzlijoumen.utils.JHConnection;
import tn.harzlijoumen.utils.JHConnectionException;

/**
 * @author Harzli Joumen
 */
public class Midlet extends MIDlet {

    Display disp = Display.getDisplay(this);

    public void startApp() {
        StringBuffer url = new StringBuffer("");
        url.append("http://localhost/votreprojet/authentification.php");
        url.append("login.php?user=a");
        url.append("&password=b");
        try {
            JHConnection conn = new JHConnection(url); // initialisation
            if (conn.validerTexte()) { // Vérification de la réponse du serveur
                if (conn.lireTexte().equals("faux")) { // lire le contenu de la reponse
                    Alert a = new Alert("Erreur", "Veuillez verfier vos données", null, AlertType.ERROR);
                    disp.setCurrent(a);
                } else {
//                    vh = new VoyageurHandler();
//                    conn.effectuerParsing(vh); // effectuer un parsing
//                    Voyageurs = vh.getVoyageurs();
//                    m.v = Voyageurs[0];
//                    m.disp.setCurrent(new Menu(m));
                    Alert a = new Alert("Erreur", "Veuillez verfier vos données", null, AlertType.ERROR);
                    disp.setCurrent(a);
                }
            }
        } catch (JHConnectionException ex) {
            Alert a = new Alert("Erreur", ex.getMessage(), null, AlertType.ERROR); // afficher une erreur pour l'utilisateur
            disp.setCurrent(a);
        }

    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
    }
}
