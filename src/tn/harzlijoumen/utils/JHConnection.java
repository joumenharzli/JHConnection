/* 
 * Copyright (C) 2015 Harzli Joumen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tn.harzlijoumen.utils;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @file JHConnection
 * @version v1.3
 * @since 18 déc. 2015
 * @author Harzli Joumen
 *
 */
public class JHConnection {

    private StringBuffer url;
    HttpConnection ht;

    public JHConnection(StringBuffer url) {
        this.url = url;
    }

    public void setUrl(StringBuffer url) {
        this.url = url;
    }

    /**
     * Retourne la reponse du serveur
     *
     * @return
     * @throws JHConnectionException
     */
    public DataInputStream lireStream() throws JHConnectionException {
        DataInputStream dt = null;
        try {
            ht = (HttpConnection) Connector.open(url.toString());
            dt = ht.openDataInputStream();
        } catch (Exception ex) {
            System.out.println("[-] Erreur JHConnection/LireStream: Verifiez votre connexion");
            System.out.println("[-] Erreur JHConnection/LireStream: " + ex.toString());
            afficherUrl();
            throw new JHConnectionException(JHConnectionConfig.MSG_CNX);
        }
        return dt;
    }

    /**
     * retourne la reponse du serveur sous forme de texte
     *
     * @return
     * @throws JHConnectionException
     */
    public String lireTexte() throws JHConnectionException {
        StringBuffer texte = new StringBuffer("");
        try {
            DataInputStream data;
            data = lireStream();
            if (data != null) {
                int ch = -1;
                while ((ch = data.read()) != -1) {
                    texte.append((char) ch);
                }
            }
            finir(data);
        } catch (IOException ex) {
            System.out.println("[-] Erreur JHConnection/lireTexte: Stream corrompu");
            System.out.println("[-] Erreur JHConnection/lireTexte: " + ex.toString());
            afficherUrl();
            throw new JHConnectionException(JHConnectionConfig.MSG_SC);
        }
        return texte.toString().trim();
    }

    /**
     * affiche la reponse du serveur
     *
     * @throws JHConnectionException
     */
    public void afficherTexte() throws JHConnectionException {
        System.out.println("[!] Info JHConnection/afficherTexte:");
        System.out.println("{" + lireTexte() + "}");
    }

    /**
     * afficher l'url
     * @throws JHConnectionException
     */
    public void afficherUrl()  throws JHConnectionException {
        System.out.println("[!] Info JHConnection/afficherUrl: {" + url.toString() + "}");
    }
    
    /**
     * effectue le parsing de la reponse du serveur
     *
     * @param handler
     * @return
     * @throws JHConnectionException
     */
    public boolean effectuerParsing(DefaultHandler handler) throws JHConnectionException {
        boolean valide = true;
        DataInputStream data;
        data = lireStream();
        if (data != null) {
            try {
                SAXParser parser;
                parser = SAXParserFactory.newInstance().newSAXParser();
                try {
                    parser.parse(data, handler);
                } catch (IOException ex) {
                    System.out.println("[-] Erreur JHConnection/EffectuerParsing: Stream corrompu");
                    System.out.println("[-] Erreur JHConnection/EffectuerParsing: " + ex.toString());
                    valide = false;
                    afficherUrl();
                    afficherTexte();
                    throw new JHConnectionException(JHConnectionConfig.MSG_SC);
                }
            } catch (ParserConfigurationException ex) {
                System.out.println("[-] Erreur JHConnection/EffectuerParsing: Impossible d'initialiser SAXParser");
                System.out.println("[-] Erreur JHConnection/EffectuerParsing: " + ex.toString());
                valide = false;
                afficherUrl();
                afficherTexte();
                throw new JHConnectionException(JHConnectionConfig.MSG_SP);
            } catch (SAXException ex) {
                System.out.println("[-] Erreur JHConnection/EffectuerParsing: Erreur de parsing");
                System.out.println("[-] Erreur JHConnection/EffectuerParsing: " + ex.toString());
                valide = false;
                afficherUrl();
                afficherTexte();
                throw new JHConnectionException(JHConnectionConfig.MSG_ErrPar);
            }
        }
        finir(data);
        return valide;
    }

    /**
     * Verifie si la reponse est vide ou nn et si elle contient le caractere 'E'
     * ==> erreur
     *
     * @return
     * @throws JHConnectionException
     */
    public boolean validerTexte() throws JHConnectionException {
        boolean valide = true;
        String texte;
        texte = lireTexte();
        if (texte.indexOf("ErreurSQL") == 0) {
            System.out.println("[-] Erreur JHConnection/validerTexte: Erreur de requete SQL");
            afficherUrl();
            valide = false;
            throw new JHConnectionException(JHConnectionConfig.MSG_conf);
        } else if (texte.length() == 0) {
            System.out.println("[!] Info JHConnection/validerTexte: Document vide");
            afficherUrl();
            valide = false;
            throw new JHConnectionException(JHConnectionConfig.MSG_VIDE);
        } else if ((texte.indexOf("Error 404") != -1) || (texte.indexOf("Object not found!") != -1)) {
            System.out.println("[!] Info JHConnection/validerTexte: le fichier n'existe pas sur le serveur");
            afficherUrl();
            valide = false;
            throw new JHConnectionException(JHConnectionConfig.MSG_404);
        } else {
            valide = true;
        }
        return valide;
    }

    /**
     * finir la connexion
     * @param dt
     * @throws JHConnectionException
     */
    public void finir(DataInputStream dt) throws JHConnectionException {
        if (dt != null) {
            try {
                dt.close();
            } catch (IOException ex) {
                System.out.println("[-] Erreur JHConnection/finir: Impossible de fermer le stream");
                System.out.println("[-] Erreur JHConnection/finir: " + ex.toString());
                afficherUrl();
                throw new JHConnectionException(JHConnectionConfig.MSG_ErrfS);
            }
        }
        if (ht != null) {
            try {
                ht.close();
            } catch (IOException ex) {
                System.out.println("[-] Erreur JHConnection/finir: Impossible de fermer la connexion");
                System.out.println("[-] Erreur JHConnection/finir: " + ex.toString());
                afficherUrl();
                throw new JHConnectionException(JHConnectionConfig.MSG_ErrfC);
            }
        }
    }
}
