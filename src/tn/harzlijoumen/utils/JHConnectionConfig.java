/* 
 * Copyright (C) 2015 Harzli Joumen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tn.harzlijoumen.utils;

/**
 *
 * @file JHConfig
 * @version v1.2
 * @since 18 déc. 2015
 * @author Harzli Joumen
 *
 */
public final class JHConnectionConfig {

    final public static String MSG_VIDE = "Vide";
    final public static String MSG_CNX = "Veuillez verifier votre connexion";
    final public static String MSG_SC = "Stream corrompu";
    final public static String MSG_SP = "Impossible d'initialiser SAXParser";
    final public static String MSG_ErrPar = "Erreur de parsing";
    final public static String MSG_conf = "Erreur de configuration";
    final public static String MSG_404 = "Impossible d’accéder à la page demandée";
    final public static String MSG_ErrfS = "Impossible de fermer le stream";
    final public static String MSG_ErrfC = "Impossible de fermer la connexion";    
}
